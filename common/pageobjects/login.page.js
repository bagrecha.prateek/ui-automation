
const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginPage extends Page {

    LoginPage(usernameInput, passwordInput, loginBtn) {
        this.usernameInput = usernameInput;
        this.passwordInput = passwordInput;
        this.loginBtn = loginBtn;
    }
    /**
     * define selectors using getter methods
     */
    get inputUsername () { return $('#usernameInput') }
    get inputPassword () { return $('#passwordInput') }
    get btnSubmit () { return $('.ui-button-text') }
    get navUsername() { return $('a.nav-link:nth-child(2)') }
    get logoutLink() { return $('[routerlink="/logout"]')}

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    login (username, password) {
        this.inputUsername.setValue(username);
        this.inputPassword.setValue(password);
        this.btnSubmit.click(); 
    }

    /**
     * overwrite specifc options to adapt it to page object
     */
    open () {
        return super.open('login');
    }

    logout() {
        this.logoutLink.click();
    }
}

module.exports = new LoginPage();
