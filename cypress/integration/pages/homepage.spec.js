/* eslint-disable @typescript-eslint/no-explicit-any */
import {loginBtn, usernameInput, passwordInput} from "./../../../common/locators"


describe("Visit DRIM homepage", () => {
    it("should redirect to login page", () => {
      cy.visit("https://demo.drim.cloud/");
      cy.get(usernameInput).type("TGorniak");
      cy.get(passwordInput).type("test123");
      cy.get(loginBtn).click();
      cy.get('[routerlink="/logout"]').should("be.visible");
      cy.get('[routerlink="/logout"]').click();
      cy.get(usernameInput).should("be.visible");
    });
  });