# Unit Testing Frameworks

The project provides following implementation of the integration testing framework
- [Cypress](https://docs.cypress.io/guides/overview/why-cypress.html#In-a-nutshell)
- [WebdriverIO](https://docs.cypress.io/guides/overview/why-cypress.html#In-a-nutshell)

### Setup

1. Both Cypress & Webdriverio need a valid package.json
2. Both require all depedencies in package.json installed on the hardware you are running the tests on.

### Prerequisites

The frameworks need working website to be already running that needs to be tested.

### Installation

Run `npm install`

#### Cypress 

##### Running tests

you can run your tests local by running the following command 

GUI  -> `npm run cypress:open` 
Headless -> `npm run cypress:headless`

#### WebdriverIO 

##### Running tests

you can run your tests local by running the following command 

GUI  -> `npm run wdio:test` 
Headless -> `npm run wdio:headless`

