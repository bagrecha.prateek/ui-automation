const LoginPage = require ('../../../common/pageobjects/login.page')

describe('DRIM Login', () => {

    beforeEach(() => {
        console.log({browser: browser});
        LoginPage.open();
    })

    describe('User Login', () => {
        it('User login into DRIM Cloud', () => {
            LoginPage.login("TGorniak", "test123");
            expect(LoginPage.logoutLink).to.be.visible()
        })
    })

    describe('User Logout', () => {
        it('User logout of DRIM Cloud', () => {
            LoginPage.logout();
            expect(LoginPage.inputUsername).to.be.visible()
        })
    })

})